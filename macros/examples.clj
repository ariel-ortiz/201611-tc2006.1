(defmacro my-and
  "Evaluates all its arguments one at a time, from left to
  right. If a form returns logical false (nil or false),
  my-and returns that value and doesn't evaluate any of the
  other expressions, otherwise it returns the value of the
  last expr. (my-and) returns true."
  ([] true)
  ([x] x)
  ([x & y]
   `(let [temp# ~x]
      (if temp#
          (my-and ~@y)
          temp#))))
        
(defmacro debug
  "Prints:  
  
    debug: original-expression => evaluated-expression
    
  And returns the value of the evaluated expression."
  [expr]
  `(let [temp# ~expr]
     (printf "debug: %s => %s%n" '~expr temp#)
     temp#))
   
(defn keep-between
  [start end lst]
  (->>
    (drop-while #(not= start %) lst)
    rest
    (take-while #(not= end %))))
  
(defmacro IF
  [condition & params]
  (let [then-part (keep-between :THEN :ELSE params)
        else-part (keep-between :ELSE :THEN params)]
    `(if ~condition 
         (do ~@then-part)
         (do ~@else-part))))
