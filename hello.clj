
; First Clojure function.
(defn hello
  "Initial hello function."
  [name]
  (str "Hello " name))