;;; Examples using higher-order functions.

(defn my-map
  "Our own implementation of the map function."
  [fun lst]
  (if (empty? lst)
      ()
      (cons (fun (first lst))
            (my-map fun (rest lst)))))
          
(defn my-reduce
  "Our own implementation of the reduce function."
  [fun init lst]
  (if (empty? lst)
      init
      (fun (first lst) (my-reduce fun init (rest lst)))))
    
(defn compose
  "Returns a function that composes f and g."
  [f g]
  (fn [x]
    (f (g x))))
  
(defn f1 [x] (* x x x))
(defn f2 [x] (+ x 2))
(def f3 (compose f1 f2))
(def f4 (compose f2 f1))
(def f5 (compose f3 f4))