; Author: Diego Galíndez
(use 'clojure.test) ; Import Clojure unit test framework.

(defn largest
  [l]
  (if (= (count l) 1)
    (first l)
    (if (> (first l) (largest (rest l)))
      (first l)
      (largest (rest l)))))

(defn decrement
  [n]
  (if (zero? n)
    ()
    (cons n (decrement (dec n)))))

(defn log2
  [n]
  (if (= 1 n)
    0
    (+ 1 (log2 (quot n 2)))))

(deftest test-largest
  (is (= -2 (largest '(-10 -5 -2 -7))))
  (is (= 42 (largest '(8 4 15 42 23 16))))
  (is (= 5 (largest '(5)))))

(deftest test-decrement
  (is (= '(5 4 3 2 1) (decrement 5)))
  (is (= '(1) (decrement 1)))
  (is (= '() (decrement 0))))

(deftest test-log2
  (is (= 10 (log2 1024)))
  (is (= 3 (log2 10)))
  (is (= 0 (log2 1))))

(run-tests)
