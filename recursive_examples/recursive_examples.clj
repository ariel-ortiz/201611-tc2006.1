(defn pow
  "Raises b to the power e."
  [b e]
  (loop [i e
         r 1N]
    (if (zero? i)
        r
        (recur (dec i) (* b r)))))
    
(defn dup
  "Returns a list with all the elements of x
  duplicated."
  [x]
  (loop [lst x
         result ()]
    (if (empty? lst)
        (reverse result)
        (recur (rest lst)
               (cons (first lst)
                     (cons (first lst) result))))))
                   
(defn add1
  "Adds one to each element of x."
  [x]
  (loop [lst    x
         result ()]
    (if (empty? lst)
        (reverse result)
        (recur (rest lst)
               (cons (inc (first lst)) result)))))