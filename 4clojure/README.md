# 4Clojure Exercises

Go to the [4clojure.com](https://www.4clojure.com/) web site and create an account. Solve the following problems.

## 2016-03-08

- [21. Nth Element](https://www.4clojure.com/problem/21)
- [34. Implement range](https://www.4clojure.com/problem/34)
- [38. Maximum value](https://www.4clojure.com/problem/38)
- [166. Comparisons](https://www.4clojure.com/problem/166)
- [81. Set Intersection](https://www.4clojure.com/problem/81)
- [88. Symmetric Difference](https://www.4clojure.com/problem/88)
- [156. Map Defaults](https://www.4clojure.com/problem/156)

