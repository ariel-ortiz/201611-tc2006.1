(use 'clojure.test)

(def p21
  
  #(first (drop %2 %1))

)

(deftest test-p21
  (is (= (p21 '(4 5 6 7) 2) 6))
  (is (= (p21 [:a :b :c] 0) :a))
  (is (= (p21 [1 2 3 4] 1) 2))
  (is (= (p21 '([1 2] [3 4] [5 6]) 2) [5 6])))

(run-tests)
